$('input[type="checkbox"]').click(function() {
    var $checked = $('input[type="checkbox"]:checked');
    var $productItem = $('tbody tr');
	  var filters = [];
    var productFilter = '';

    if ($checked.length > 0) {
        $productItem.hide();

        $checked.each(function() {
           dataType = $(this).attr('data-type');
           if (!filters[dataType]) filters[dataType] = [];
           filters[dataType].push(this.value);
        });

    	for(var filterType in filters) {
            for (i in filters[filterType]) {
            	productFilter += '[data-'+filterType+'="'+filters[filterType][i]+'"],';
            }
            productFilter = productFilter.replace(/,$/,'');
        }
        $productItem
        	.filter(productFilter)
            .map(function() {
             	$(this).show();
         	});
    } else {
        $productItem.show();
    }
});
