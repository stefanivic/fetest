var map;

function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
        center: {
            lat: 44.011192,
            lng: 20.916273
        },
        zoom: 8
    });
    var htLarge = {
        url: 'img/sprite/HireTest-sprite.png',
        size: new google.maps.Size(167, 180),
        origin: new google.maps.Point(166, 161)
    }
    var htMedium = {
        url: 'img/sprite/HireTest-sprite.png',
        size: new google.maps.Size(113, 115),
        origin: new google.maps.Point(175, 375)
    }
    var htSmall = {
        url: 'img/sprite/HireTest-sprite.png',
        size: new google.maps.Size(68, 64),
        origin: new google.maps.Point(179, 487)
    }
    var places = [
        ['Bondi Beach', 44.011192, 20.916273, htLarge],
        ['Coogee Beach', 44.021332, 22.461512, htMedium],
        ['Cronulla Beach', 43.723667, 20.679442, htSmall],
        ['Manly Beach', 44.347897, 21.464625, htSmall],
        ['Maroubra Beach', -33.950198, 151.259302, htMedium]
    ];
    // Create markers.
    var shape = {
        coords: [1, 1, 1, 20, 18, 20, 18, 1],
        type: 'poly'
    };

    for (var i = 0; i < places.length; i++) {
        var place = places[i];
        var marker = new google.maps.Marker({
            position: {
                lat: place[1],
                lng: place[2]
            },
            map: map,
            icon: place[3],
            shape: shape,
            title: place[0]
        });
    }
}
