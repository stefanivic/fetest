// ---------------------------------
// Gulp plugins
// ---------------------------------
var gulp = require('gulp');
var sass = require('gulp-sass');
var browserSync = require('browser-sync').create();
var useref = require('gulp-useref');
var uglify = require('gulp-uglify');
var gulpIf = require('gulp-if');
var imagemin = require('gulp-imagemin');
var cache = require('gulp-cache');
var runSequence = require('run-sequence');
var sourcemaps = require('gulp-sourcemaps');
var autoprefixer = require('gulp-autoprefixer');
var cleanCSS = require('gulp-clean-css');

// ---------------------------------
// Variables
// ---------------------------------
var autoprefixerOptions = {
  browsers: ['last 2 versions', '> 5%', 'Firefox ESR']
};

var sassOptions = {
  errLogToConsole: true,
  outputStyle: 'expanded'
};

// ---------------------------------
// Tasks
// ---------------------------------
gulp.task('browserSync', function () {
  browserSync.init({
    server: {
      baseDir: 'dist'
    }
  });
});

gulp.task('sass', function () {
  gulp.src('src/scss/*.scss')
    .pipe(sourcemaps.init())
    .pipe(sass(sassOptions).on('error', sass.logError))
    .pipe(sourcemaps.write())
    .pipe(autoprefixer(autoprefixerOptions))
    .pipe(gulp.dest('dist/css'))
    .pipe(browserSync.reload({stream:true}));
});

gulp.task('minify-css', function () {
  gulp.src('dist/css/*.css')
    .pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(gulp.dest('dist/css/'));
});

gulp.task('images', function () {
  gulp.src('src/img/**/*.+(png|jpg|gif|svg)')
    .pipe(cache(imagemin([
      imagemin.jpegtran({progressive: true}),
	    imagemin.optipng({optimizationLevel: 5}),
    ])))
    .pipe(gulp.dest('dist/img'));
});

gulp.task('fonts', function () {
  gulp.src('src/fonts/**/*')
    .pipe(gulp.dest('dist/fonts'));
});

gulp.task('watch', ['browserSync'], function () {
  gulp.watch('src/scss/**/*.scss', ['sass']);
  gulp.watch('dist/*.html').on('change', browserSync.reload);
  gulp.watch('dist/js/*.js').on('change', browserSync.reload);
});

gulp.task('build', function (callback) {
  runSequence(['images', 'fonts', 'minify-css']);
});

gulp.task('default', function (callback) {
  runSequence(['sass', 'browserSync', 'watch']);
});
